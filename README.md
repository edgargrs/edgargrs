# Hi 🖖 My name is Edgar GRS

## React Native Developer

- 🌍 I'm based in México
- ✉️ You can contact me at [hi@edgargrs.com](mailto:hi@edgargrs.com)
- 🚀 I'm currently working on some cool project.
- 🧠 I'm learning Typescript
- 🤝 I'm open to collaborating on interesting projects
